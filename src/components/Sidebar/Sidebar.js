import React, { useState, useEffect } from "react";
import { Button as ButtonBase, Drawer, IconButton, List } from "@material-ui/core";
import {
  Home as HomeIcon,
  NotificationsNone as NotificationsIcon,
  FormatSize as TypographyIcon,
  FilterNone as UIElementsIcon,
  BorderAll as TableIcon,
  QuestionAnswer as SupportIcon,
  LibraryBooks as LibraryIcon,
  HelpOutline as FAQIcon,
  ArrowBack as ArrowBackIcon,
  Work as WorkIcon,
  Receipt as ReceiptIcon,
  MenuBook as MenuBookIcon,
  Storage as StorageIcon,
  Settings as SettingsIcon
} from "@material-ui/icons";
import { useTheme } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import { mdiRotate3d } from '@mdi/js';
import Icon from '@mdi/react'
import classNames from "classnames";

// styles
import useStyles from "./styles";

// components
import SidebarLink from "./components/SidebarLink/SidebarLink";
import Dot from "./components/Dot";

// context
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar,
} from "../../context/LayoutContext";

// https://www.youtube.com/watch?v=W-sZo6Gtx_E

const structure = [
  {
    id: 0,
    label: "Home",
    link: "/app/dashboard",
    icon: <HomeIcon />
  },
  {
    id: 1,
    label: "My Files",
    link: "/app/my-files",
    icon: <StorageIcon />,
    // children: [
    //   { label: "Icons", link: "/app/ui/icons" },
    //   { label: "Charts", link: "/app/ui/charts" },
    //   { label: "Maps", link: "/app/ui/maps" },
    // ],
  },
  {
    id: 2,
    label: "Settings",
    link: "/app/settings",
    icon: <SettingsIcon /> },
  {
    id: 3,
    label: "Catalogue",
    link: "/app/catalogue",
    icon: <MenuBookIcon />,
  },
  {
    id: 4,
    label: "Projects",
    link: "/app/projects",
    icon: <WorkIcon />,
  },

  // { id: 5, type: "divider" },
  // { id: 6, type: "title", label: "HELP" },
  // { id: 7, label: "peepee", link: "https://flatlogic.com/templates", icon: <LibraryIcon /> },
  // { id: 8, label: "poopoo", link: "https://flatlogic.com/forum", icon: <SupportIcon /> },
  // { id: 9, label: "FAQ", link: "https://flatlogic.com/forum", icon: <FAQIcon /> },
  { id: 10, type: "divider" },
  { id: 11, type: "title", label: "Last Projects" },
  {
    id: 12,
    label: "My recent",
    link: "",
    icon: <Dot size="small" color="primary" />,
  },
  {
    id: 13,
    label: "Starred",
    link: "",
    icon: <Dot size="small" color="primary" />,
  },
  {
    id: 14,
    label: "Projects",
    link: "",
    icon: <Dot size="small" color="primary" />,
  },
];

function Sidebar({ location }) {
  var classes = useStyles();
  var theme = useTheme();

  // global
  var { isSidebarOpened } = useLayoutState();
  var layoutDispatch = useLayoutDispatch();

  // local
  var [isPermanent, setPermanent] = useState(true);

  useEffect(function() {
    window.addEventListener("resize", handleWindowWidthChange);
    handleWindowWidthChange();
    return function cleanup() {
      window.removeEventListener("resize", handleWindowWidthChange);
    };
  });

  return (
    <Drawer
      variant={isPermanent ? "permanent" : "temporary"}
      className={classNames(classes.drawer, {
        [classes.drawerOpen]: isSidebarOpened,
        [classes.drawerClose]: !isSidebarOpened,
      })}
      classes={{
        paper: classNames({
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        }),
      }}
      open={isSidebarOpened}
    >
      <div className={classes.toolbar} />
      <div className={classes.mobileBackButton}>
        <IconButton onClick={() => toggleSidebar(layoutDispatch)}>
          <ArrowBackIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse),
            }}
          />
        </IconButton>
      </div>
      <List className={classes.sidebarList}>
        {structure.map(link => (
          <SidebarLink
            key={link.id}
            location={location}
            isSidebarOpened={isSidebarOpened}
            {...link}
          />
        ))}
      </List>

      <ButtonBase variant="contained" size="large" color="secondary">
        <Icon
          path={mdiRotate3d}
          size={1}
        />&nbsp;
        3D EDITOR
      </ButtonBase>
    </Drawer>
  );

  // ##################################################################
  function handleWindowWidthChange() {
    var windowWidth = window.innerWidth;
    var breakpointWidth = theme.breakpoints.values.md;
    var isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  }
}

export default withRouter(Sidebar);
