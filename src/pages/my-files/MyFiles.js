import React, { useState } from "react";
import {
  Grid,
  LinearProgress,
  Select,
  OutlinedInput,
  MenuItem,
  Button,
} from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import { createTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MUIDataTable from "mui-datatables";


// components
import Widget from "../../components/Widget";
import PageTitle from "../../components/PageTitle";
import { Typography } from "../../components/Wrappers";
import Dot from "../../components/Sidebar/components/Dot";
import { AxiosProvider, Request, Get, Delete, Head, Post, Put, Patch, withAxios } from 'react-axios';

export default function MyFiles(props) {
  // var classes = useStyles();
  var theme = useTheme();

  // local
  var [mainChartState, setMainChartState] = useState("monthly");

  return (
    <>
      <PageTitle title="My Files"
      variant="contained"
      size="medium"
      color="secondary"/>
      <Grid container spacing={4}>
        <Grid item xd={12} md={12} lg={12}>
          {/* <Widget title="react-axios demo" disableWidgetMenu> */}
          <span>
            <Get url="/api/user" params={{id: "12345"}}>
              {(error, response, isLoading, makeRequest, axios) => {
                if(error) {
                  // console.log('request error:');
                  // console.log(error);
                  return (
                    <>
                    <MuiThemeProvider theme={
                      createTheme({
                        overrides: {
                          MUIDataTableHeadCell: {
                            fixedHeader: {
                              // Header cr
                              backgroundColor: '#DFEFDD'
                            },
                          },
                          MUIDataTableSelectCell: {
                            fixedHeader: {
                              backgroundColor: '#DFEFDD'
                            }
                          },
                          // works
                          MUIDataTableBodyCell: {
                            root: {
                              // backgroundColor: '#DFEFDD',
                            }
                          }
                        }
                      })
                    }>
                      <MUIDataTable
                        title="Stored Files"
                        data={[
                          ["Lamp.obj", "OBJ object file.", "25.060 KB", "mein eigenes Label A", "08-11-2021 | 14:05:12"],
                          ["asdf.obj", "OBJ object file", "5.404 KB", "mein eigenes Label B", "08-10-2021 | 09:55:37"],
                          ["akai_mpk_mini.stp", "STEP file", "44.804 KB", "-", "04-20-2021 | 06:09:55"]
                        ]}
                        columns={["Filename", "Type", "Size", "Label", "Last changed"]}
                        options={{
                          filterType: "checkbox",
                        }}
                      />
                    </MuiThemeProvider>
                    {/* <div>Something bad happened: {error.message} <button onClick={() => makeRequest({ params: { reload: true } })}>Retry</button></div> */}
                    </>
                  );
                }
                else if(isLoading) {
                  return (<div>Loading...</div>)
                }
                else if(response !== null) {
                  console.log('response:');
                  console.log(response);
                  return (<div>{response.data.message} <button onClick={() => makeRequest({ params: { refresh: true } })}>Refresh</button></div>)
                }
                return (<div>Default message before request is made.</div>)
              }}
            </Get>
          </span>
          {/* </Widget> */}
        </Grid>
      </Grid>
    </>
  );
}